const config = drupalSettings.devel.codemirror;

config.mode = "text/x-php";
config.tabSize = 2;

const codeMirror = CodeMirror.fromTextArea(
  document.getElementById("edit-code"),
  config
);
codeMirror.getWrapperElement().style.fontSize = codeMirror.getTextArea().style.fontSize;
