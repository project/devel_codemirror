<?php

namespace Drupal\devel_codemirror\Form;

use Drupal\Core\Cache\CacheCollectorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DevelCodemirrorSettingsForm.
 */
class DevelCodemirrorSettingsForm extends ConfigFormBase {

  /**
   * The cache collector service.
   *
   * @var \Drupal\Core\Cache\CacheCollectorInterface
   */
  protected $cacheCollector;

  /**
   * The extension list service.
   *
   * @var \Drupal\Core\Extension\ExtensionList
   */
  protected $extensionList;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a DevelCodemirrorSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Cache\CacheCollectorInterface $cache_collector
   *   The cache collector service.
   * @param \Drupal\Core\Extension\ExtensionList $extension_list
   *   The extension list service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheCollectorInterface $cache_collector, ExtensionList $extension_list, FileSystemInterface $file_system) {
    parent::__construct($config_factory);

    $this->cacheCollector = $cache_collector;
    $this->extensionList = $extension_list;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('library.discovery.collector'),
      $container->get('extension.list.module'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'devel_codemirror_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('devel_codemirror.settings');
    $themes = $this->getThemes();

    $form['theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Theme'),
      '#description' => $this->t('The theme to style the editor with.'),
      '#options' => array_combine($themes, $themes),
      '#empty_value' => 'default',
      '#default_value' => $config->get('theme'),
    ];
    $form['lineWrapping'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Line wrapping'),
      '#description' => $this->t('Whether CodeMirror should scroll or wrap for long lines.'),
      '#default_value' => $config->get('lineWrapping'),
    ];
    $form['lineNumbers'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Line number'),
      '#description' => $this->t('Whether to show line numbers to the left of the editor.'),
      '#default_value' => $config->get('lineNumbers'),
    ];
    $form['matchBrackets'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Match brackets'),
      '#description' => $this->t('When set to true or, causes matching brackets to be highlighted whenever the cursor is next to them.'),
      '#default_value' => $config->get('matchBrackets'),
    ];
    $form['autoCloseBrackets'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto close brackets'),
      '#description' => $this->t('Auto-close brackets and quotes when typed.'),
      '#default_value' => $config->get('autoCloseBrackets'),
    ];
    $form['styleActiveLine'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Style active line'),
      '#default_value' => $config->get('styleActiveLine'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $form_state->cleanValues();

    $this->config('devel_codemirror.settings')
      ->setData($form_state->getValues())
      ->save();

    $this->cacheCollector->clear();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'devel_codemirror.settings',
    ];
  }

  /**
   * Gets list of themes.
   *
   * @return array
   *   An array of available themes.
   */
  private function getThemes() {
    $path = $this->extensionList->getPath('devel_codemirror') . '/codemirror/theme';
    $themes = array_keys($this->fileSystem->scanDirectory($path, '/.*\.css$/', [
      'recurse' => FALSE,
      'key' => 'name',
    ]));
    asort($themes);
    return $themes;
  }

}
